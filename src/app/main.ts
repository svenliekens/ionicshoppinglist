import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);

export const environment = {
    production: false,
    firebase: {
      apiKey: 'AIzaSyDfc8mBWYyQJknhthVxsBR0euS3YBv2QeY',
      authDomain: 'shoppinglist-78809.firebaseapp.com',
      databaseURL: 'https://shoppinglist-78809.firebaseio.com',
      projectId: 'shoppinglist-78809',
      storageBucket: 'shoppinglist-78809.appspot.com',
      messagingSenderId: '524741748766'
    }
  };
