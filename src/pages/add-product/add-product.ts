import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform } from 'ionic-angular';

import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import {AngularFireDatabase} from 'angularfire2/database';
import { Item } from '../../models/item';
import { AngularFireAuth } from 'angularfire2/auth';
import { Vibration } from '@ionic-native/vibration';

@IonicPage()
@Component({
  selector: 'page-add-product',
  templateUrl: 'add-product.html',
})
export class AddProductPage {
  code:string;
  name:string;
  list1Ref;
  items;

  constructor(public vibration: Vibration, public afAuth: AngularFireAuth, public db:AngularFireDatabase, public platform: Platform, public toast:ToastController, public barcode:BarcodeScanner, public navCtrl: NavController, public navParams: NavParams) {
    this.getDataFromDB(this.afAuth.auth.currentUser.uid);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddProductPage');
  }

  // Als de gebruiker op de scan knop drukt
  scanNewProduct(){
    if(this.platform.is('cordova')){
      // Gebruik native barcode scanner
      this.barcode.scan({showTorchButton:true, orientation: 'portrait'}).then((barcodeData) => {
        this.vibration.vibrate(600);
        // Vul het barcode inputveld in met de barcode die gescand werd
        this.code = barcodeData.text;
      });
    }
  }

  // Methode om een nieuw product toe te voegen met de ingevulde data
  addProduct(){
    // Checken of de velden wel ingevuld zijn
    if(this.code == undefined || this.name == undefined || this.code.trim() == ""|| this.name.trim() == ""){
      // Als er een of meerdere velden niet ingevuld zijn klagen we hierover in een toast (ook een native functie)
      let toast = this.toast.create({
        message: 'Both fields are required',
        duration: 3000
      });
      toast.present();
    }else{
      // Als alle velden ingevuld zijn maken we hieruit een nieuw item en voegen we het aan de DB toe
      let item = new Item(this.name, this.code);
      item.setNeeded(false);
      this.addItemToList(this.afAuth.auth.currentUser.uid, item);  
    }
  }

  // Methode om een item aan de DB toe te voegen
  addItemToList(listID, item){
    this.items.push(item);
    this.db.object(`lists/${listID}`).set(this.items);
    this.navCtrl.pop();
  }

  // Methode om de lijst met items op te halen voordat we er een nieuwe aan toe voegen
  getDataFromDB(listID){
    this.list1Ref = this.db.database.ref(`lists/${listID}`);
      this.list1Ref.on('value', snap =>{
        let currentList = [];
        if(snap.val() != null){
          currentList = snap.val();  
        }
        this.items = currentList;
      });
  }
}