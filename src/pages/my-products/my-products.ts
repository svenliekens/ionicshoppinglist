import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';

import {AddProductPage} from '../../pages/add-product/add-product';

import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Vibration } from '@ionic-native/vibration';

@IonicPage()
@Component({
  selector: 'page-my-products',
  templateUrl: 'my-products.html',
})
export class MyProductsPage {
  items;
  list1Ref;
  
  constructor(public vibration: Vibration, public toastCtrl:ToastController, public barcodeScanner: BarcodeScanner, public alertCtrl: AlertController, public afAuth: AngularFireAuth, public db: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams) {
    this.items = [];
  }

  // Naar de add product page gaan als de gebruiker een nieuw product wilt aanmaken
  addNewProduct(){
    this.navCtrl.push(AddProductPage);
  }

  // Methode om een product in te scannen zodat het op het boodschappenlijstje gezet wordt
  scanProductToList(){
    this.barcodeScanner.scan({showTorchButton:true, orientation: 'portrait'}).then((barcodeData) => {
      this.vibration.vibrate(600);
      // Gescande barcode ophalen
      let barcode = barcodeData.text;
      // Zoeken naar een product met deze barcode
      let index = this.items.findIndex((item) => {return item.barcode == barcode});

      let toastMessage = "Product not found"
      if(index > -1){
        // Als het product gevonden wordt voegen we het toe aan de boodschappenlijst
        let item = this.items[index];
        toastMessage = item.name + " added to list";
        this.addToShoppingList(this.afAuth.auth.currentUser.uid, item)
      }

      // Toast met info tonen
      let toast = this.toastCtrl.create({
        message: toastMessage,
        duration: 3000
      });
      toast.present();
    });
  }

  // Methode om lijst van producten op te halen
  getDataFromDB(listID){
    this.db.database.ref(`lists/${listID}`).on('value', snap =>{
        let currentList = [];
        if(snap.val() != null){
          currentList = snap.val();  
        }
        this.items = currentList;
      });
  }

  // Prompt tonen wanneer de gebruiker op een product klikt
  showProductDetails(item){
    let alert = this.alertCtrl.create({
      title: item.name,
      buttons: [
        
        {
          text: 'Need',
          handler: data => {
            this.addToShoppingList(this.afAuth.auth.currentUser.uid, item);
          }
        },
        {
          text: 'Delete',
          handler: data => {
            this.deleteItem(this.afAuth.auth.currentUser.uid, item);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
        
      ]
    });
    alert.present();
  }

  // Gegevens refreshen wanneer de gebruiker terug in de pagina komt
  ionViewWillEnter(){
    this.getDataFromDB(this.afAuth.auth.currentUser.uid);
  }

  // Methode om item uit de DB te verwijderen
  deleteItem(listID, item){
    // Naar het item zoeken a.d.h.v barcode en naam
    let test = this.items.findIndex((i) => {return (i.name == item.name) && (i.barcode == item.barcode)});

    if(test > -1){
      // Het item verwijderen als het bestaat
      this.items.splice(test, 1);
      this.db.object(`lists/${listID}`).set(this.items);
    }
  }

  // Methode om product aan boodschappenlijst toe te voegen
  addToShoppingList(listID, item){
    // Item zoeken in de lijst van items
    let index = this.items.findIndex((i) => {return (i.name == item.name) && (i.barcode == item.barcode)});

    if(index > -1){
      // Als het item bestaat passen we het aan zodat het in het lijstje komt
      this.items[index].needed = true;
      this.db.object(`lists/${listID}`).set(this.items);
    }
  }
}