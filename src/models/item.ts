// Data model voor een item/product
export class Item { 
    needed: false;
    name: "defaultItem";
    barcode: string;

    constructor(name, barcode){
        this.name = name;
        this.barcode = barcode;
    }

    setNeeded(needed){
        this.needed = needed;
    }
}