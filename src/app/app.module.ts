import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import {ShoppingListPage} from '../pages/shopping-list/shopping-list';
import {MyProductsPage} from '../pages/my-products/my-products';
import {SharingPage} from '../pages/sharing/sharing';
import {SettingsPage} from '../pages/settings/settings';
import {AddProductPage} from '../pages/add-product/add-product';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Vibration } from '@ionic-native/vibration';
import {ScreenOrientation} from '@ionic-native/screen-orientation';

import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule, AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';


export const firebaseConfig = {
  apiKey: "AIzaSyDfc8mBWYyQJknhthVxsBR0euS3YBv2QeY",
  authDomain: "shoppinglist-78809.firebaseapp.com",
  databaseURL: "https://shoppinglist-78809.firebaseio.com",
  storageBucket: "shoppinglist-78809.appspot.com",
  messagingSenderId: '524741748766'
};

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    ShoppingListPage,
    MyProductsPage,
    SharingPage,
    SettingsPage,
    AddProductPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    ShoppingListPage,
    MyProductsPage,
    SharingPage,
    SettingsPage,
    AddProductPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    Vibration,
    AngularFireDatabase,
    ScreenOrientation,
    AuthProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
