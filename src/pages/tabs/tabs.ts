import { Component } from '@angular/core';

import {ShoppingListPage} from '../shopping-list/shopping-list';
import {MyProductsPage} from '../my-products/my-products';
import {SharingPage} from '../sharing/sharing';
import {SettingsPage} from '../settings/settings';

import {ScreenOrientation} from '@ionic-native/screen-orientation';

import {Platform} from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  shoppingList = ShoppingListPage;
  myProducts = MyProductsPage;
  sharing = SharingPage;
  settings = SettingsPage;

  constructor(private screenOrientation: ScreenOrientation, public platform: Platform) {
    if(this.platform.is('cordova')){
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
  }
}