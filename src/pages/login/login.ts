import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, Loading, AlertController} from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email';

import {TabsPage} from '../tabs/tabs';
import {ScreenOrientation} from '@ionic-native/screen-orientation';

import {Platform} from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  loading: Loading;

  constructor(public navCtrl: NavController, public authData: AuthProvider, 
              public formBuilder: FormBuilder, public alertCtrl: AlertController,
              public loadingCtrl: LoadingController, private screenOrientation: ScreenOrientation, public platform: Platform) 
  {
    if(this.platform.is('cordova')){
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, 
        EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), 
      Validators.required])]
    });  
  }

  // Doorverwijzen naar registreerpagina als er op de knop gedrukt wordt
  createAccount(){
    this.navCtrl.push("SignupPage");
  }

  // Methode om in te loggen als er op de login knop gedrukt wordt
  loginUser(){
    if(!this.loginForm.valid){
      console.log(this.loginForm.value);
    } else { 
      // Als de gegevens geldig zijn wordt er via de authprovider ingelogd
      this.authData.loginUser(this.loginForm.value.email, this.loginForm.value.password)
      .then(authData => {
        // Na het inloggen wordt de gebruiker naar de homepage van de app gebracht
        this.navCtrl.setRoot(TabsPage);
      }, error => {
        this.loading.dismiss().then(()=> {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons:[
              {
                text:"Ok",
                role: 'cancel'
              }
            ]
          });  
          alert.present(); 
        });
      });
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,  
      });
      this.loading.present();
    }
  }
  
  // Doorverwijzen naar paswoord reset pagina als er op de knop gedrukt wordt
  goToResetPassword(){
    this.navCtrl.push("ResetPasswordPage");
  }
}