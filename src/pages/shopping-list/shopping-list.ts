import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import {BarcodeScanner} from '@ionic-native/barcode-scanner';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the ShoppingListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html',
})
export class ShoppingListPage {
  items;
  list1Ref;
  sub;

  constructor(public vibration: Vibration, public afAuth:AngularFireAuth, public toastCtrl: ToastController, public alertCtrl: AlertController, public db:AngularFireDatabase, public navCtrl: NavController, public barcodeScanner: BarcodeScanner, public navParams: NavParams) {
    this.getDataFromDB(this.afAuth.auth.currentUser.uid);
  }

  ionViewWillEnter() {
    this.getDataFromDB(this.afAuth.auth.currentUser.uid);
  }

  // Methode om product uit de lijst te scannen
  scan() {
    this.barcodeScanner.scan({showTorchButton:true, orientation: 'portrait'}).then((barcodeData) => {
      this.vibration.vibrate(600);
      // Barcode data ophalen
      let barcode = barcodeData.text;
      // Het item zoeken
      let index = this.items.findIndex((item) => {return item.barcode == barcode});

      // Berichtje opbouwen
      let toastMessage = "Product not found"
      if(index > -1){
        // Als het product bestaat, dit van de shopping list halen
        let item = this.items[index];
        toastMessage = item.name + " removed from list";
        this.removeFromShoppingList(this.afAuth.auth.currentUser.uid, item);
      }

      // Toast tonen
      let toast = this.toastCtrl.create({
        message: toastMessage,
        duration: 3000
      });
      toast.present();
    });
  }

  // Methode om producten op te halen
  getDataFromDB(listID){
    this.list1Ref = this.db.database.ref(`lists/${listID}`);
      this.list1Ref.on('value', snap =>{
        let currentList = [];
        if(snap.val() != null){
          currentList = snap.val();  
        }
        this.items = currentList;
      });
  }

  // Dialog tonen als er op een item geklikt wordt
  itemClick(item){
    let alert = this.alertCtrl.create({
      title: item.name,
      buttons: [
        
        {
          text: "Don't need",
          handler: data => {
            this.removeFromShoppingList(this.afAuth.auth.currentUser.uid, item);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
        
      ]
    });
    alert.present();  
  }

  // Methode om product uit shopping list te verwijderen
  removeFromShoppingList(listID, item){
    let index = this.items.findIndex((i) => {return (i.name == item.name) && (i.barcode == item.barcode)});

    if(index > -1){
      this.items[index].needed = false;
      this.db.object(`lists/${listID}`).set(this.items);
    }
  }
}