// Data model voor gebruiker
export class User { 
    uid: "";
    email: "";
    shoppinglist: number;
    
    constructor(uid, email){
        this.uid = uid;
        this.email = email;
    }
  }