import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import {LoginPage} from '../login/login';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  public loading: Loading;

  constructor(public app : App, public loadingCtrl:LoadingController, public auth:AuthProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  // Knop om uit te loggen
  logOut(){
    // Laadanimatie tonen
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    
    // Uitloggen via de authprovider
    this.auth.logoutUser().then(()=>{
      this.app.getRootNav().setRoot(LoginPage).then(()=>{
        this.loading.dismiss();
      });
    });
    
  }

}
