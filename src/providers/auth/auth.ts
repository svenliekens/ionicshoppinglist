import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

import { User } from '../../models/user';

@Injectable()
export class AuthProvider { 
  userValueChangedSubscription;

  constructor(public db: AngularFireDatabase, public afAuth: AngularFireAuth){
  }

  // Methode om gebruiker te registeren
  signupUser(newEmail: string, newPassword: string): Promise<any>{
    // Registreren via angularfire2
    let registerPromise = this.afAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword).then(()=>{
      // Gebruiker in realtime db zetten als hij er nog niet in staat.
      let user = new User(this.afAuth.auth.currentUser.uid, this.afAuth.auth.currentUser.email);
      this.addUserToDbIfNotExists(user);
    });
  
    return registerPromise;
  }

  // Methode om gebruiker in te loggen
  loginUser(newEmail: string, newPassword: string): Promise<any> {
    // Inloggen via angularfire2
    let loginPromise = this.afAuth.auth.signInWithEmailAndPassword(newEmail, newPassword).then(() => {
      // Gebruiker in realtime db zetten als hij er nog niet in staat.
      let user = new User(this.afAuth.auth.currentUser.uid, this.afAuth.auth.currentUser.email);
      this.addUserToDbIfNotExists(user);
    });
    
    return loginPromise;
  }

  // Methode om gebruiker uit te loggen
  logoutUser(): Promise<void> {
    // valueChanges event loskoppelen
    if(this.userValueChangedSubscription != undefined){
      this.userValueChangedSubscription.unsubscribe();
    }
    // uitloggen via angularfire2
    return this.afAuth.auth.signOut();
  }

  // Methode om paswoord te resetten
  resetPassword(email: string): Promise<void>{
    // paswoord reset via angularfire2
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  // Methode om gebruiker aan realtime db toe te voegen als hij nog niet bestaat
  addUserToDbIfNotExists(user){
    this.userValueChangedSubscription = this.db.object(`/users/${user.uid}`).valueChanges().subscribe(u => {
      if(u == undefined){
        console.log("User not found in DB, adding it now...")
        this.db.object(`/users/${user.uid}`).update(user).then(()=> {
          console.log("User added to DB")
        });
      }else{
        console.log("User found in DB");
      }
    });
  }
}
